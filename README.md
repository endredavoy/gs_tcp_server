#Python TCP Server

This is a tcp server for a portable ground station with GNU-Radio and USRP

The python file __gs\_tcp_server.py__ _runs a server that the GNU-radio software can connect to.

Clients that are going to transmit data (Uplink) are called TX clients and are connected to the port 8000

Clients that are receiving data (Downlink) are called RX clients and are connected to the port 8100

These "Clients" are inside the GNU-radio

The GNU-Radio Companion file __duplex\_tcp-test.grc__ is the program that runs the USRP radio and connects to the server

#Prerequisites 

You need to download and install Gnu-radio Companion and the Nuts module

Python 2.7

A USRP radio connected and configured

#Setup:

1. Clone the git repository

2. Cd into gs\_tcp\_server/TcpServer: __cd gs\_tcp\_server/TcpServer__

3. Run __gs\_tcp_server.py__: __python gs\_tcp_server.py__

4. Run gnuradio-companion and open duplex\_tcp-test.grc

5. Compile and run the program

The server logs the files to /Logs/_'address:port'.txt_
