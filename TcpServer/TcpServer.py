
import socket
import sys
import threading
import select
import time
import os

class Server(object):


    _listen_socket = None
    _clients = {}
    _nextid = 0
    _threads = []

    #Boolean that states whether the server is running or shutting down
    _running = True

    #Time inbetween connection validation
    CONNECTION_VALIDATION_TIMING = 10.0 

    port = 8000
    address = 'localhost'

    def __init__(self):


        print("Starting server...")

        #Creates a new socket to listen to
        self._listen_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self._listen_socket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)


        #binds the socket to a ip address and port.
        self._listen_socket.bind((self.address,self.port))

        self._listen_socket.setblocking(False)

        #Start listening to connections
        self._listen_socket.listen(1)
    
        #Creates a thread that checks for connections
        connection_thread = threading.Thread(name="Connection Checker", target=self.check_for_connections)
        connection_thread.start()
        self._threads.append(connection_thread)


        print("Listening for connections")
        #Creates a thread that handle input
        handle_input = threading.Thread(name="Input_Handler",target=self.handle_input)
        handle_input.start()
        self._threads.append(handle_input)
        

        #Creates a thread that receives packages from clients and log the data
        rx_thread = threading.Thread(name="RX Thread", target=self.receive_from_clients)
        rx_thread.start()
        self._threads.append(rx_thread)

        print("Shut down the server by typing: 'q' or 'exit' ")
        print("Send a message by typing anything else")

        #This just makes sure the main thread runs indefinatly until we want
        #the server to quit
        while(self._running):
            try:
                time.sleep(0.01)
                continue
            except KeyboardInterrupt:
                self._running = False
                break


        #This code runs when the server is shutting down

        for cl in self._clients.values():
			#closes the socket, disconnecting the client
            cl.socket.close()
        self._listen_socket.close()

        
        #This makes the program wait for all the thread to shut down
        for thread in self._threads:
            thread.join()

        print("Server shutted down")
        exit()




    #Runs on a separate thread and takes in terminal input
    def handle_input(self):
        while(self._running):

            time.sleep(0.01)
            try:
                message = ""
                if(sys.version < '3'):
                    message = raw_input()
                else:
                    message = input()

            except EOFError:
                return
            #Checks for commands first
            if(message.lower() in ("exit","q")):
                self._running = False

            #Send the input string to all clients
            for id,cl in list(self._clients.items()):
                self.attempt_send(id,message)



    #This function receives data from all connected clients
    def receive_from_clients(self):
        while (self._running):
            time.sleep(0.01)


            for id,client in self._clients.items():
                if(client == None):
                    self.handle_disconnection(id)
                    break

                #This checks if the client is still connected
                if(not time.time() - client.lastcheck < self.CONNECTION_VALIDATION_TIMING):
                   self.attempt_send(id,"\x00")
                   client.lastcheck = time.time()

                # we use 'select' to test whether there is data waiting to be
                # read from
                # the client socket.  The function takes 3 lists of sockets,
                # the
                # first being
                # those to test for readability.  It returns 3 list of sockets,
                # the
                # first being
                # those that are actually readable.
                try:
                    if(self._running and client != None and client.socket != None and not client.socket._closed):
                        rlist,wlist,xlist = select.select([client.socket],[],[],0)
                except ValueError: return

                if(client.socket not in rlist): continue

                try:
                    #receive data from the client
                    data = client.socket.recv(4096)
                    
                    print("Received data from address",client.address, ":")
                    print(data)
                    self.append_log(data,time.asctime(),client)
                    #self._attempt_send(id,str(data, "latin1"))

                    continue
                except socket.error as e:
                    print(e.strerror)
                    self.handle_disconnection(id)
                    break

    #Appends log data to the client's log file
    def append_log(self, data, time, client):
        
        file = open(client.log,'a')
        file.write('Data received at: ' + time + '\n')
        try:
            file.write('    ' + str(data) + '\n \n')
        except TypeError:
            file.write('    ' + 'Data is not convertable to text' + '\n \n')

        file.close()


    #Tries to send data to the client
    def attempt_send(self,clid,data):
        if sys.version < '3' and type(data) != unicode: data = unicode(data,"latin1")
        try:
            #sends the data
            self._clients[clid].socket.sendall(bytearray(data,"latin1"))

        except KeyError: pass

        except TypeError as e:
            print(e)
        #socket.error is raised if there is a connection problem or the client
        #has
        #disconnected
        except socket.error:
            self.handle_disconnection(clid)


   


    #This runs when a client has disconnected or when a socket error occurs
    def handle_disconnection(self,id):
    
        print("Client with address " + self._clients[id].address + " has disconnected")
        self._clients[id].socket.close()
        #Delete the client
        del(self._clients[id])



    #Checks for connecting clients, accept the clients if they are connecting
    #and adding them to the list
    def check_for_connections(self):
        while(self._running):

            time.sleep(0.01)

            # 'select' is used to check whether there is data waiting to be
            # read
            # from the socket.  We pass in 3 lists of sockets, the first being
            # those
            # to check for readability.  It returns 3 lists, the first being
            # the sockets that are readable.  The last parameter is how long to
            # wait -
            # we pass in 0 so that it returns immediately without waiting
            try:
                if(self._running and self._listen_socket != None):
                    rlist,wlist,xlist = select.select([self._listen_socket],[],[],0)
            except ValueError: return
            
            # if the socket wasn't in the readable list, there's no data
            # available,
            # meaning no clients waiting to connect, and so we can exit the
            # method
            # here
            if self._listen_socket not in rlist: continue

            print("A new client is connecting")

            # 'accept' returns a new socket and address info which can be used
            # to
            # communicate with the new client
            joined_socket,addr = self._listen_socket.accept()

            # set non-blocking mode on the new socket.  This means that 'send'
            # and
            # 'recv' will return immediately without waiting
            joined_socket.setblocking(False)

            

            # construct a new _Client object to hold info about the newly
            # connected
            # client.  Use 'nextid' as the new client's id number
            self._clients[self._nextid] = Client(joined_socket,addr[0],"",time.time())

            print("New connection established from address ", addr[0])

            self.append_log ("Client connected at ",time.asctime(),self._clients[self._nextid])

            self._nextid += 1
        


    

#Containing the Client helper object
class Client(object):

    socket = None
    address = ""
    buffer = ""
    lastcheck = 0

    log_dir = os.getcwd() +"/Logs/"
    log = None

    def __init__(self,socket,address,buffer,lastcheck):
        self.socket = socket
        self.address = address
        self.buffer = buffer
        self.lastcheck = lastcheck
        self.log = self.log_dir+'Log_' + address + '.txt'
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)
           
        

if(__name__ == "__main__"):
    server = Server()


