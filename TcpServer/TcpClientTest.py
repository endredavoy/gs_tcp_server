import socket
import sys
import time
import threading
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('localhost', 8100)

def connect(server_adress):
    try:
        print ( 'connecting to ', server_address)
        _sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        _sock.connect(server_address)

        return _sock
        print("Connected to client")
    except socket.error as e:
        print(e.strerror)
        print("Error number: ", e.errno)
        sock.close()
        return sock
       
            
            


def shutdown():
    sock.close()

def echo():
    while(1):
        try:

            data = int(str(sock.recv(4096),'latin1')) + 1
            print(data)
            sock.sendall(bytearray(str(data), 'latin1'))
        except Exception as e:
            print(e)
            

        time.sleep(1)


echoThread = threading.Thread(name ="Echo Thread",target = echo)
echoThread.start()

while(1):
    
    try:
        # Send data
        sock.sendall(bytearray(' ', 'latin1'))
        
    except socket.error as e:
         print(e.strerror)
         sock = connect(server_address)
    except:
        print("Something went wrong. Oh no.")
   
    time.sleep(5)


shutdown()