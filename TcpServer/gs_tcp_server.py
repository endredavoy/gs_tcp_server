
import socket
import sys
import threading
import select
import time
import os
import glob


class GS_Server(object):


    _listen_socket_tx = None
    _listen_socket_rx = None
    _rx_clients = []
    _tx_clients = []
    _threads = []
    
    #variable that contains the data that has just been transmitted
    _sent_data = None
    
    _time_last_transmission = 0

    #Boolean that states whether the server is running or shutting down
    _running = True

    
    DUPLEX_ECHO_DELAY = 0.5

    tx_port = 8000
    rx_port = 8100
    address = 'localhost'

    def __init__(self):


        print("Starting server...")

        #Creates a new socket to listen to
        self._listen_socket_tx = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self._listen_socket_tx.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)

        self._listen_socket_rx = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self._listen_socket_rx.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
        
        

        #binds the socket to a ip address and port.
        self._listen_socket_tx.bind((self.address,self.tx_port))
        self._listen_socket_rx.bind((self.address,self.rx_port))

        self._listen_socket_tx.setblocking(False)
        self._listen_socket_rx.setblocking(False)

        #Start listening to connections
        self._listen_socket_tx.listen(1)
        self._listen_socket_rx.listen(1)

        
        #Creates a thread that checks for connections
        rx_connection_thread = threading.Thread(name="Connection Checker", target=self.check_for_connections,args=(self._listen_socket_rx,self._rx_clients))
        rx_connection_thread.start()
        self._threads.append(rx_connection_thread)

         #Creates a thread that checks for connections
        tx_connection_thread = threading.Thread(name="Connection Checker", target=self.check_for_connections,args=(self._listen_socket_tx,self._tx_clients))
        tx_connection_thread.start()
        self._threads.append(tx_connection_thread)


        print("Listening for connections")
        #Creates a thread that handle input
        handle_input = threading.Thread(name="Input_Handler",target=self.handle_input)
        handle_input.start()
        self._threads.append(handle_input)
        

        #Creates a thread that receives packages from clients and log the data
        rx_thread = threading.Thread(name="RX Thread", target=self.receive_from_clients)
        rx_thread.start()
        self._threads.append(rx_thread)

        print("Shut down the server by typing: 'exit' ")
        print("Clear all logs by typing 'clear logs'")
        print("Send a message by typing anything else")

        #This just makes sure the main thread runs indefinatly until we want
        #the server to quit
        while(self._running):
            try:
                time.sleep(0.01)
                continue
            except KeyboardInterrupt:
                self._running = False
                break


        #This code runs when the server is shutting down

        for cl in self._rx_clients:
			#closes the socket, disconnecting the client
            cl.socket.close()
        self._listen_socket_rx.close()

        for cl in self._tx_clients:
		    #closes the socket, disconnecting the client
            cl.socket.close()
        self._listen_socket_tx.close()

        
        #This makes the program wait for all the thread to shut down
        for thread in self._threads:
            thread.join()

        print("Server shutted down")
        exit()




    #Runs on a separate thread and takes in terminal input
    def handle_input(self):
        while(self._running):

            time.sleep(0.01)
            try:
                message = ""
                if(sys.version < '3'):
                    message = raw_input()
                else:
                    message = input()

            except EOFError:
                return
            #Handles exit commands
            if(message.lower() == "exit"):
                self._running = False
            #Handles clear logs command
            elif(message.lower() == 'clear logs'):
                y = ''
                if(sys.version < '3'):
                     y = raw_input("Are you sure you want to delete all data logs?")
                else:
                     y = input("Are you sure you want to delete all data logs?")

                if(y in ('y','Y','yes','Yes')):
                    files = glob.glob(os.getcwd() + '/Logs/*')
                    for f in files:
                        os.remove(f)
                    print("Cleared all logs")
                else:
                    print('Cancelled')

            else:
                #Send the input string to all TX clients
                for cl in self._tx_clients:
                    self.attempt_send(cl,message)



    #This function receives data from all connected RX clients
    def receive_from_clients(self):
        while (self._running):
            time.sleep(0.01)

            if(self._sent_data != None and not time.time() - self._time_last_transmission < self.DUPLEX_ECHO_DELAY):
                self._sent_data = None

            for client in self._rx_clients:
                if(client == None):
                    self.handle_disconnection(client)
                    break

                #This checks if the client is still connected
                #if(not time.time() - client.lastcheck < self.CONNECTION_VALIDATION_TIMING):
                #   self.attempt_send(client,"\x00")
                #   client.lastcheck = time.time()

                # we use 'select' to test whether there is data waiting to be
                # read from
                # the client socket.  The function takes 3 lists of sockets,
                # the
                # first being
                # those to test for readability.  It returns 3 list of sockets,
                # the
                # first being
                # those that are actually readable.
                try:
                    if(self._running and client != None and client.socket != None):
                        rlist,wlist,xlist = select.select([client.socket],[],[],0)
                except ValueError: return

                if(client.socket not in rlist): continue

                try:
                    #receive data from the client
                    data = client.socket.recv(4096)
                    
                    if(self._sent_data != None and self._sent_data != '\0' and data == self._sent_data):
                        print("Received echo data: " + data)
                    elif (data != '\0' and data != ' ' and data != '\000' and data != ''):
                        print("Received data from address" + str(client.address) +  ":" + str( client.socket.getsockname()[1]))
                        print(data)
                        self.append_log(data,time.asctime(),client)
                    

                    continue
                except socket.error as e:
                    print(e.strerror)
                    self.handle_disconnection(client)
                    break

    #Appends log data to the client's log file
    def append_log(self, data, time, client):
        
        file = open(client.log,'a')
        file.write('Log entry:' + time + '\n')
        try:
            file.write('    ' + str(data) + '\n \n')
        except TypeError:
            file.write('    ' + 'Data is not convertable to text' + '\n \n')

        file.close()


    #Tries to send data to the client
    def attempt_send(self,client,data):
        if sys.version < '3' and type(data) != unicode: data = unicode(data,"latin1")
        try:
            #sends the data
            client.socket.sendall(bytearray(data,"latin1"))
            self._sent_data = bytearray(data,"latin1")
            self._time_last_transmission = time.time()
            self.append_log('Sent data: '+data,time.asctime(),client)
        except KeyError: pass

        except TypeError as e:
            print(e)
        #socket.error is raised if there is a connection problem or the client
        #has
        #disconnected
        except socket.error, e:
            print(e)
            if(e.errno != 32):
                self.handle_disconnection(client)


   


    #This runs when a client has disconnected or when a socket error occurs
    def handle_disconnection(self,client):
    
        print("Client with address " + client.address + " has disconnected")
        client.socket.close()
        #Delete the client
        if(client in self._rx_clients):
            self._rx_clients.remove(client)
        if(client in self._tx_clients):
            self._tx_clients.remove(client)

    #Checks for connecting clients, accept the clients if they are connecting
    #and adding them to the list
    def check_for_connections(self, socket, list):
        while(self._running):

            time.sleep(0.01)

            # 'select' is used to check whether there is data waiting to be
            # read
            # from the socket.  We pass in 3 lists of sockets, the first being
            # those
            # to check for readability.  It returns 3 lists, the first being
            # the sockets that are readable.  The last parameter is how long to
            # wait -
            # we pass in 0 so that it returns immediately without waiting
            try:
                if(self._running and socket != None):
                    rlist,wlist,xlist = select.select([socket],[],[],0)
            except ValueError: return
            
            # if the socket wasn't in the readable list, there's no data
            # available,
            # meaning no clients waiting to connect, and so we can exit the
            # method
            # here
            if socket not in rlist: continue

            print("A new client is connecting")

            # 'accept' returns a new socket and address info which can be used
            # to
            # communicate with the new client
            
            joined_socket,addr = socket.accept()

            # set non-blocking mode on the new socket.  This means that 'send'
            # and
            # 'recv' will return immediately without waiting
            joined_socket.setblocking(False)

            

            # construct a new Client object to hold info about the newly
            # connected Client5
            list.append(Client(joined_socket,addr[0],"",time.time()))
            
            print("New connection established from address "+ addr[0] + ":" +str(socket.getsockname()[1]))

            self.append_log ("Client connected with address "+ addr[0] +':'+ str(socket.getsockname()[1]),time.asctime(),list[-1])

        


    

#Containing the Client helper object
class Client(object):

    socket = None
    address = ""
    buffer = ""
    lastcheck = 0

    log_dir = os.getcwd() +"/Logs/"
    log = None

    def __init__(self,socket,address,buffer,lastcheck):
        self.socket = socket
        self.address = address
        self.buffer = buffer
        self.lastcheck = lastcheck
        self.log = self.log_dir+'Log_' + address + ':'+ str(socket.getsockname()[1])+ '.txt'
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)
           
        

if(__name__ == "__main__"):
    server = GS_Server()


